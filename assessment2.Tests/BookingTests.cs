﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using assessment2;

namespace assessment2.Tests
{
    [TestClass]
    public class BookingTests
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrivalDateIsLessThanTodaysDate_ThrowException()
        {
            List<Guest> guests = new List<Guest>();
            guests.Add(new Guest("Gosho", "12345", 20));

            Booking booking = new Booking(1, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(2),
                guests, new List<Extra>());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DepartureDateIsLessThanArrivalDate_ThrowException()
        {
            List<Guest> guests = new List<Guest>();
            guests.Add(new Guest("Gosho", "12345", 20));

            Booking booking = new Booking(1, DateTime.Now.AddDays(3) , DateTime.Now,
                guests, new List<Extra>());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NumberOfGuestsIs0_ThrowException()
        {
            List<Guest> guests = new List<Guest>();

            Booking booking = new Booking(1, DateTime.Now, DateTime.Now.AddDays(3),
                guests, new List<Extra>());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NumberOfGuestsIsMoreThan4_ThrowException()
        {
            List<Guest> guests = new List<Guest>();
            
            for(int i = 0; i < 5; i++)
            {
                guests.Add(new Guest("Guest", "12345", 20));
            }

            Booking booking = new Booking(1, DateTime.Now, DateTime.Now.AddDays(3),
                guests, new List<Extra>());
        }

        [TestMethod]
        public void GetCostMethodShouldReturn460()
        {
            List<Guest> guests = new List<Guest>();
            guests.Add(new Guest("Gosho", "12345", 20));
            guests.Add(new Guest("BatStamo", "12345", 16));

            List<Extra> extras = new List<Extra>();
            extras.Add(new Breakfast(""));
            extras.Add(new EveningMeal(""));
            extras.Add(new CarHire(DateTime.Now, DateTime.Now.AddDays(2),
                "ShishoBakshisho"));

            Customer customer = new Customer("Kolio", "Na mainata si");

            //Add to the list to pass the CustomerRefNumber validation in the Booking class
            Storage.Customers.Add(customer);

            Booking booking = new Booking(customer.ReferenceNumber, DateTime.Now, DateTime.Now.AddDays(3),
                guests, extras);

            decimal totalCost = booking.GetCost();

            Storage.Customers.Remove(customer);

            Assert.AreEqual<decimal>(460, totalCost);
        }
    }
}
