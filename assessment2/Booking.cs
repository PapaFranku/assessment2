﻿#region Summary
/*
 * Author: Asparuh Kamenov 02/12/2016
 * 
 * Description: This class represents a booking. It includes an ID for the booking, arrival and departure date
 * of the guests, customer ID and name, a list of guests and a list of optional extras. It also includes a method which
 * calculates the total price of the booking. The Clone() method allows copying of the object.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public class Booking : ICloneable
    {
        #region Properties
        private readonly int referenceNumber;
        /// <summary>
        /// Gets the booking' ID.
        /// </summary>
        public int ReferenceNumber
        {
            get { return referenceNumber; }
        }

        /// <summary>
        /// Gets the customer name associated with the booking.
        /// </summary>
        public string CustomerName
        {
            get 
            {
                int index = Storage.GetIndexAtID(DatabaseOptions.Customer, customerRefNumber);

                return Storage.Customers[index].Name;
            }
        }

        private DateTime arrivalDate;
        /// <summary>
        /// Gets or sets the arrival date for the booking.
        /// </summary>
        public DateTime ArrivalDate
        {
            get { return arrivalDate; }
            set 
            {
                if (value >= DateTime.Now.Date)
                {
                    arrivalDate = value;
                }
                else
                {
                    throw new Exception("Invalid date!");
                }
            }
        }

        private DateTime departureDate;
        /// <summary>
        /// Gets or sets the departure date for the booking.
        /// </summary>
        public DateTime DepartureDate
        {
            get { return departureDate.Date; }
            set 
            {
                if(value.Date >= DateTime.Now.Date && value.Date > arrivalDate.Date)
                {
                    departureDate = value.Date;
                }
                else
                {
                    throw new Exception("Invalid date!");
                }
            }
        }

        private int customerRefNumber;
        /// <summary>
        /// Gets or sets the ID of the customer on whose name the booking is created.
        /// </summary>
        public int CustomerRefNumber
        {
            get { return customerRefNumber; }
            set 
            {
                bool idExists = false;
                foreach(Customer c in Storage.Customers)
                {
                    if(value == c.ReferenceNumber)
                    {
                        customerRefNumber = value;
                        idExists = true;
                        break;
                    }
                }

                if(!idExists)
                {
                    throw new Exception("No customer with the given ID exists!");
                }
            }
        }

        private List<Guest> guests;
        /// <summary>
        /// Gets or sets the guests for the booking.
        /// </summary>
        public List<Guest> Guests
        {
            get 
            {
                return guests;
            }

            set
            {
                if (value.Count <= 4 && value.Count != 0)
                {
                    guests = value;
                }
                else
                {
                    throw new Exception("Maximum guests allowed is 4 and at least one guest must be added!");
                }
            }
        }

        private List<Extra> extras;
        /// <summary>
        /// Gets or sets the optional extras for a booking.
        /// </summary>
        public List<Extra> Extras
        {
            get { return extras; }
            set { extras = value; }
        }
        #endregion

        public Booking(int customerRefNumber, DateTime arrivalDate, DateTime departureDate,
            List<Guest> guests, List<Extra> extras)
        {
            this.Guests = guests;
            this.Extras = extras;
            this.CustomerRefNumber = customerRefNumber;
            this.ArrivalDate = arrivalDate;
            this.DepartureDate = departureDate;
            this.referenceNumber = Storage.GetID(DatabaseOptions.Booking);
        }

        #region Methods
        /// <summary>
        /// Returns the total cost for the booking.
        /// </summary>
        /// <returns></returns>
        public decimal GetCost()
        {
            decimal cost = 0;
            int nights = (departureDate.Date - arrivalDate.Date).Days;

            foreach(Guest g in guests)
            {
                if(g.Age <= 18)
                {
                    cost += 30 * nights;
                }
                else
                {
                    cost += 50 * nights;
                }
            }

            if (extras.Count != 0)
            {
                foreach (Extra e in extras)
                {
                    if(e is CarHire)
                    {
                        CarHire carHire = e as CarHire;

                        int daysHired = (carHire.ReturnDate.Date - carHire.HireDate.Date).Days;

                        cost += carHire.Cost * daysHired;
                    }
                    else
                    {
                        cost += (e.Cost * guests.Count) * nights;
                    }
                }
            }

            return Math.Round(cost, 2);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion
    }
}