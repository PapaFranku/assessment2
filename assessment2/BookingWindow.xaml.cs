﻿#region Summary
/*
 * Author: Asparuh Kamenov 02/12/2016
 * 
 * Part of the Prototype design pattern.
 * Description: A window from where bookings can be recorded and amended. It uses a page system for the guests,
 * each page representing one of the 4 possible guests. The two arrow buttons can be used to scroll through the pages.
 * If a page is empty that means that there isn't a guest recorded at that index and thus there is space for more. 
 * Otherwise the guest details are displayed and can be amended. The "page" variable represents the current page. 
 * The "itemAtPage" 2D array represents one of the four pages and the index which stores the guest object
 * from the "guests" List: itemAtPage[pageNumber]["guests" index]. The zero argument constructor is used to call the
 * window when creating a new booking. Else, the second constructor is used and the argument passed corresponds to the
 * index of the observable collection in the "Storage" class, which holds all existing bookings. The "currentBooking"
 * object is null when invoking the zero argument constructor. Else it retrieves a prototype object from the 
 * "Storage" class. When clicking the save button if "currentBooking" is null, a new Booking object is created with
 * the values from the GUI. Else the "currentBooking" replaces the object in the collection in the "Storage" class.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for BookingWindow.xaml
    /// </summary>
    public partial class BookingWindow : Window
    {
        //Represents the current page
        private int page = 0;
        //Represents the index of the item in the "guests" list. If there isn't an item the value is less than 0
        private int[,] itemAtPage;

        private Booking currentBooking;
        private List<Guest> guests;
        private List<Extra> extras;

        //Used when creating a booking
        public BookingWindow()
        {
            InitializeComponent();

            itemAtPage = new int[4,1];
            guests = new List<Guest>(4);
            extras = new List<Extra>(3);

            //Set the indices to -1
            for (int i = 0; i < 4; i++)
            {
                itemAtPage[i, 0] = -1;
            }
        }

        //Used when amending a booking
        public BookingWindow(int index)
        {
            InitializeComponent();

            //Set the current booking as the one which is to be amended
            currentBooking = (Booking)Storage.Bookings[index].Clone();
            guests = currentBooking.Guests;
            extras = currentBooking.Extras;
            itemAtPage = new int[4, 1];

            //Set the indices to -1
            for(int i = 0; i < 4; i++)
            {
                itemAtPage[i, 0] = -1;
            }

            //Add the indices to the array for every item in "guests"
            for (int i = 0; i < guests.Count; i++)
            {
                itemAtPage[i, 0] = i;
            }

            //Set the values to the template
            dp_ArrivalDate.SelectedDate = currentBooking.ArrivalDate;
            dp_DepartureDate.SelectedDate = currentBooking.DepartureDate;

            int customerIndex = Storage.GetIndexAtID(DatabaseOptions.Customer, currentBooking.CustomerRefNumber);
            txtblBooking_CustomerName.Text = Storage.Customers[customerIndex].Name;
            txtblBooking_CustomerID.Text = currentBooking.CustomerRefNumber.ToString();

            displayGuestDetails();
            displayExtras();
        }

        #region Events
        //When the "Save Booking" button is clicked
        private void btnBooking_Add_Click(object sender, RoutedEventArgs e)
        {
            if(dp_ArrivalDate.SelectedDate == null || dp_DepartureDate.SelectedDate == null)
            {
                MessageBox.Show("Select the arrival and departure dates!", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            try
            {
                //Add the values to the "extras" if the chekboxes are checked
                if(chkboxCustomer_Breakfast.IsChecked == true)
                {
                    Breakfast breakfast = new Breakfast(txtbExtra_BreakfastDietaryReq.Text);
                    extras.Add(breakfast);
                }

                if(chkboxCustomer_Dinner.IsChecked == true)
                {
                    EveningMeal meal = new EveningMeal(txtbExtra_DinnerDietaryReq.Text);
                    extras.Add(meal);
                }

                if(chkboxCustomer_CarHire.IsChecked == true)
                {
                    if(dp_CarFrom.SelectedDate == null || dp_CarTo.SelectedDate == null)
                    {
                        MessageBox.Show("Select hire start and end dates!", "Error",
                            MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        return;
                    }

                    CarHire carHire = new CarHire(dp_CarFrom.SelectedDate.Value.Date, dp_CarTo.SelectedDate.Value.Date,
                        txtbExtra_DriverName.Text);
                    extras.Add(carHire);
                }

                //If adding a new booking booking
                if (currentBooking == null)
                {
                    currentBooking = new Booking(int.Parse(txtblBooking_CustomerID.Text), 
                        dp_ArrivalDate.SelectedDate.Value.Date, dp_DepartureDate.SelectedDate.Value.Date, guests, extras);

                    Storage.Bookings.Add(currentBooking);
                }
                else
                {
                    //Set the values to the currentBooking
                    currentBooking.CustomerRefNumber = int.Parse(txtblBooking_CustomerID.Text);
                    currentBooking.ArrivalDate = dp_ArrivalDate.SelectedDate.Value.Date;
                    currentBooking.DepartureDate = dp_DepartureDate.SelectedDate.Value.Date;
                    currentBooking.Guests = guests;
                    currentBooking.Extras = extras;

                    int index = Storage.GetIndexAtID(DatabaseOptions.Booking, currentBooking.ReferenceNumber);

                    Storage.Bookings[index] = currentBooking;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            this.Close();
        }

        //When the "->" button is clicked
        private void btnGuest_ScrollForward_Click(object sender, RoutedEventArgs e)
        {
            //Set the page to the next one, since we are going forward
            page++;

            if (btnGuest_ScrollBack.IsEnabled == false)
            {
                btnGuest_ScrollBack.IsEnabled = true;
            }
            //Disable the button if we are at the last page, since only 4 guests can be added
            if (page == 3)
            {
                btnGuest_ScrollForward.IsEnabled = false;
            }

            lblGuest_GuestNo.Content = (page + 1).ToString();

            displayGuestDetails();
        }

        //When the "<-" button is clicked
        private void btnGuest_ScrollBack_Click(object sender, RoutedEventArgs e)
        {
            //Set the page to the previous one, since we are going back
            page--;

            if (btnGuest_ScrollForward.IsEnabled == false)
            {
                btnGuest_ScrollForward.IsEnabled = true;
            }

            //Disable the button if we are at the first page
            if (page == 0)
            {
                btnGuest_ScrollBack.IsEnabled = false;
            }

            lblGuest_GuestNo.Content = (page + 1).ToString();

            displayGuestDetails();
        }

        //When the "Select Customer" button is clicked
        private void btnBooking_SelectCustomer_Click(object sender, RoutedEventArgs e)
        {
            //Browse the existing customers to assign one to the current booking
            SearchDisplayWindow window = new SearchDisplayWindow();
            window.ShowDialog();

            //If the user has selected a customer from the datagrid
            if (window.SelectedIndex >= 0)
            {
                int index = window.SelectedIndex;

                txtblBooking_CustomerName.Text = Storage.Customers[index].Name;
                txtblBooking_CustomerID.Text = Storage.Customers[index].ReferenceNumber.ToString();
            }
        }

        private void btnGuest_Remove_Click(object sender, RoutedEventArgs e)
        {
            //Call the function with the current page
            removeItemAt(page);
        }

        private void btnGuest_Add_Click(object sender, RoutedEventArgs e)
        {
            int age = 0;
            //Validate that the value is a number
            try
            {
                age = int.Parse(txtbGuest_Age.Text);
            }
            catch
            {
                MessageBox.Show("Invalid age value!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Guest guest;

            try
            {
                string fullName = txtbGuest_FirstName.Text + " " + txtbGuest_LastName.Text;

                guest = new Guest(fullName, txtbGuest_PassportNum.Text, age);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //If no exceptions, add the guest to the "guests"
            guests.Add(guest);

            btnGuest_Remove.IsEnabled = true;
            btnGuest_Add.IsEnabled = false;

            //Set the index of the current page to the max index of the "guests"
            itemAtPage[page, 0] = guests.Count - 1;
        }

        private void chkboxCustomer_Breakfast_Checked(object sender, RoutedEventArgs e)
        {
            txtbExtra_BreakfastDietaryReq.IsEnabled = true;
        }

        private void chkboxCustomer_Breakfast_Unchecked(object sender, RoutedEventArgs e)
        {
            txtbExtra_BreakfastDietaryReq.IsEnabled = false;
            txtbExtra_BreakfastDietaryReq.Clear();

            //Remove the extra from the list if it is there
            for (int i = 0; i < extras.Count; i++)
            {
                if (extras[i] is Breakfast)
                {
                    extras.RemoveAt(i);
                }
            }
        }

        private void chkboxCustomer_Dinner_Checked(object sender, RoutedEventArgs e)
        {
            txtbExtra_DinnerDietaryReq.IsEnabled = true;
        }

        private void chkboxCustomer_Dinner_Unchecked(object sender, RoutedEventArgs e)
        {
            txtbExtra_DinnerDietaryReq.IsEnabled = false;
            txtbExtra_DinnerDietaryReq.Clear();

            //Remove the extra from the list if it is there
            for(int i = 0; i < extras.Count; i++)
            {
                if(extras[i] is EveningMeal)
                {
                    extras.RemoveAt(i);
                }
            }
        }

        private void chkboxCustomer_CarHire_Checked(object sender, RoutedEventArgs e)
        {
            dp_CarFrom.IsEnabled = true;
            dp_CarTo.IsEnabled = true;
            txtbExtra_DriverName.IsEnabled = true;
        }

        private void chkboxCustomer_CarHire_Unchecked(object sender, RoutedEventArgs e)
        {
            dp_CarFrom.IsEnabled = false;
            dp_CarFrom.SelectedDate = null;
            dp_CarTo.IsEnabled = false;
            dp_CarTo.SelectedDate = null;
            txtbExtra_DriverName.IsEnabled = false;
            txtbExtra_DriverName.Clear();

            //Remove the extra from the list if it is there
            for (int i = 0; i < extras.Count; i++)
            {
                if (extras[i] is CarHire)
                {
                    extras.RemoveAt(i);
                }
            }
        }

        private void btnBooking_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion 

        #region Methods
        private void removeItemAt(int index)
        {
            //Remove the item corresponding to the index, stored in the 2D array
            guests.RemoveAt(itemAtPage[index, 0]);

            //Change the values in the array to correspond to the indices of the changed "guests" List
            //If the there was only 1 item(index 0 in the "guests" List), just set that page to be empty
            if (guests.Count == 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (itemAtPage[i, 0] == 0)
                    {
                        itemAtPage[i, 0] = -1;
                    }
                }
            }
            else if (itemAtPage[index, 0] == getMaxValue())
            {
                //If the value to be removed was the highest index in the "guests" List
                //No need to alter the other values, since they haven't changed
                itemAtPage[index, 0] = -1;
            }
            else
            {
                //If the value to be removed wasn't the highest index in the "guests" List
                for (int i = 0; i < 4; i++)
                {
                    //If the current page is the one at which the value was removed, set it to be empty (a negative value)
                    if (index == i)
                    {
                        itemAtPage[index, 0] = -1;
                    }
                    else if (itemAtPage[i, 0] > 0)
                    {
                        //Else decrement the value if there is a guest in it, since the "guests" List has shrunk
                        itemAtPage[i, 0]--;
                    }
                }
            }

            clearGuestTemplate();
        }

        //Helper function to the removeItemAt function
        //Gets the highest index held in the 2D array
        private int getMaxValue()
        {
            int max = 0;
            for(int i = 0; i < 4; i++)
            {
                if(itemAtPage[i , 0] > max)
                {
                    max = itemAtPage[i, 0];
                }
            }

            return max;
        }

        //Displays the guest details if a guest exists at that page
        private void displayGuestDetails()
        {
            //If there is a valid index, fill the boxes with the values from the "guests" List
            if (itemAtPage[page, 0] >= 0)
            {
                int index = itemAtPage[page, 0];
                string names = guests[index].Name;
                string[] splitNames = names.Split(new char[] { ' ' });

                txtbGuest_FirstName.Text = splitNames[0];
                txtbGuest_LastName.Text = splitNames[1];
                txtbGuest_PassportNum.Text = guests[index].PassportNumber;
                txtbGuest_Age.Text = guests[index].Age.ToString();
                btnGuest_Remove.IsEnabled = true;
                btnGuest_Add.IsEnabled = false;
            }
            else
            {
                //If no guest at that page, the boxes are empty
                clearGuestTemplate();
            }
        }

        //Goes through the "extras" List and sets the values in GUI, which correspond to the given extra
        private void displayExtras()
        {
            if (extras.Count != 0)
            {
                foreach (Extra extra in extras)
                {
                    if (extra is Breakfast)
                    {
                        Breakfast breakfast = extra as Breakfast;
                        chkboxCustomer_Breakfast.IsChecked = true;
                        txtbExtra_BreakfastDietaryReq.Text = breakfast.DietaryRequirements;
                    }
                    else if (extra is EveningMeal)
                    {
                        EveningMeal meal = extra as EveningMeal;
                        chkboxCustomer_Dinner.IsChecked = true;
                        txtbExtra_DinnerDietaryReq.Text = meal.DietaryRequirements;
                    }
                    else
                    {
                        CarHire carHire = extra as CarHire;
                        chkboxCustomer_CarHire.IsChecked = true;
                        dp_CarFrom.SelectedDate = carHire.HireDate;
                        dp_CarTo.SelectedDate = carHire.ReturnDate;
                        txtbExtra_DriverName.Text = carHire.DriverName;
                    }
                }
            }
        }

        //Clears the text boxes for the guest
        private void clearGuestTemplate()
        {
            txtbGuest_FirstName.Text = "";
            txtbGuest_LastName.Text = "";
            txtbGuest_PassportNum.Text = "";
            txtbGuest_Age.Text = "";
            btnGuest_Remove.IsEnabled = false;
            btnGuest_Add.IsEnabled = true;
        }
        #endregion
    }
}