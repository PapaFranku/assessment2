﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Represents the breakfast extra option. Inherits its "Cost" property from the "Extra" class.
 * Has one string property which hold the dietary requirements if there are any.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public class Breakfast : Extra
    {
        #region Properties
        private string dietaryReq;
        /// <summary>
        /// Gets or sets the dietary requirements for the breakfast extra.
        /// </summary>
        public string DietaryRequirements
        {
            get { return dietaryReq; }
            set { dietaryReq = value; }
        }
        #endregion

        public Breakfast(string dietaryReq)
        {
            Cost = 5;
            this.dietaryReq = dietaryReq;
        }
    }
}
