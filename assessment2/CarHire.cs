﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Represents the car hire extra option. Inherits its "Cost" property from the "Extra" class.
 * Has properties for the hire start and end dates and a driver name.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public class CarHire : Extra
    {
        #region Properties
        private DateTime hireDate;
        /// <summary>
        /// Gets or sets the hire start date for the car hire.
        /// </summary>
        public DateTime HireDate
        {
            get { return hireDate; }
            set
            {
                if (value >= DateTime.Now.Date)
                {
                    hireDate = value;
                }
                else
                {
                    throw new Exception("Invalid date!");
                }
            }
        }

        private DateTime returnDate;
        /// <summary>
        /// Gets or sets the hire return date for the car hire.
        /// </summary>
        public DateTime ReturnDate
        {
            get { return returnDate; }
            set
            {
                if (value.Date >= DateTime.Now.Date)
                {
                    returnDate = value.Date;
                }
                else
                {
                    throw new Exception("Invalid date!");
                }
            }
        }

        private string driverName;
        /// <summary>
        /// Gets or sets the driver' name.
        /// </summary>
        public string DriverName
        {
            get { return driverName; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    driverName = value;
                }
                else
                {
                    throw new Exception("Name cannot be empty!");
                }
            }
        }
        #endregion
        public CarHire(DateTime hireDate, DateTime returnDate, string driverName)
        {
            Cost = 50;
            this.HireDate = hireDate;
            this.ReturnDate = returnDate;
            this.DriverName = driverName;
        }
    }
}
