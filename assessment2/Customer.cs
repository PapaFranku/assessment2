﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Represents a customer on whose name bookings can be created. Inherits its "Name" property from
 * the "Person" class. Has properties for a unique ID and address. The Clone() method allows copying of the object.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public class Customer : Person, ICloneable
    {
        #region Properties
        private readonly int referenceNumber;
        public int ReferenceNumber
        {
            get { return referenceNumber; }
        }

        private string address;
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        #endregion

        public Customer(string name, string address) : base(name)
        {
            this.referenceNumber = Storage.GetID(DatabaseOptions.Customer);
            this.address = address;
        }

        #region Methods
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion
    }
}
