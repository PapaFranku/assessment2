﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Provides the means to manage existing customers. Has a data grid, connected to the observable collection
 * in the "Storage" class which holds all existing customers. A customer may be deleted or selected for amending. 
 * This window is also used by the "BookingWindow" to select a customer who will be associated to a booking.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for SearchDisplayWindow.xaml
    /// </summary>
    public partial class SearchDisplayWindow : Window
    {
        public int SelectedIndex = -1;

        public SearchDisplayWindow()
        {
            InitializeComponent();
            //Connect the "AutoGeneratingColumn" to the datagrid event handler
            dgView.AutoGeneratingColumn += 
                new EventHandler<DataGridAutoGeneratingColumnEventArgs>(this.AutoGeneratingColumn);
        }

        #region Events
        private void AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if(e.PropertyName.StartsWith("Reference"))
            {
                e.Column.Header = "ID";
            }
        }

        private void gridMain_Loaded(object sender, RoutedEventArgs e)
        {
            dgView.ItemsSource = Storage.Customers;
            //Set the last column of the datagrid to expand to the end of the datagrid
            dgView.Columns[dgView.Columns.Count - 1].Width = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            if (dgView.SelectedIndex != -1)
            {
                //Set the selected index in the datagrid to the "SelectedIndex" variable
                SelectedIndex = dgView.SelectedIndex;
                this.Close();
            }
            else
            {
                //If no item is selected as to close the window anyway.
                MessageBoxResult prompt = MessageBox.Show("No record was selected.\nClose window?", "Warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                if (prompt == MessageBoxResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dgView.SelectedIndex == -1)
            {
                MessageBox.Show("You must select a customer to delete first!", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                //If an item is selected ask the user to confirm deletion
                MessageBoxResult prompt = MessageBox.Show("Delete the selected record?", "Warning",
                        MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                if (prompt == MessageBoxResult.Yes)
                {
                    //Set the ID to the ID of the object in the collection int the "Storage" class
                    int customerID = Storage.Customers[dgView.SelectedIndex].ReferenceNumber;

                    //If the customer is associated with any bookings then the record is not removed
                    foreach (Booking b in Storage.Bookings)
                    {
                        if(customerID == b.CustomerRefNumber)
                        {
                            MessageBox.Show("Cannot delete a customer with existing bookings.", "Error",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }

                    //If the customer has no bookings remove
                    Storage.Customers.RemoveAt(dgView.SelectedIndex);
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
