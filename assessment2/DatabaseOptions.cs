﻿#region Summary
/*
 * Author: Asparuh Kamenov 02/12/2016
 * The enum is used to refer to the observable collections in the "Storage" class: One for the bookings and
 * one for the customers.
 */
#endregion
namespace assessment2
{
    public enum DatabaseOptions
    {
        Booking,
        Customer
    }
}
