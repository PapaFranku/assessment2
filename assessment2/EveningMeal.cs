﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Represents the evening meal extra option. Inherits its "Cost" property from the "Extra" class.
 * Has one string property which hold the dietary requirements if there are any.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public class EveningMeal : Extra
    {
        #region Properties
        private string dietaryReq;
        /// <summary>
        /// Gets or sets the dietary requirements for the evening meal extra.
        /// </summary>
        public string DietaryRequirements
        {
            get { return dietaryReq; }
            set { dietaryReq = value; }
        }
        #endregion

        public EveningMeal(string dietaryReq)
        {
            Cost = 15;
            this.dietaryReq = dietaryReq;
        }
    }
}
