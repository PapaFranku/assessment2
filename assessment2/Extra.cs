﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Represents the base class for the extras. Has a property which represents the cost of the extra.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public abstract class Extra
    {
        private decimal cost;
        /// <summary>
        /// Gets or sets the cost for the extra.
        /// </summary>
        public decimal Cost
        {
            get { return cost; }
            set { cost = value; }
        }
    }
}
