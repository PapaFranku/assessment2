﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: Represents a guest. Inherits its "Name" property from the "Person" class. Has properties for
 * the passport number and age of the guest.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public class Guest : Person
    {
        #region Properties
        private string passportNumber;
        /// <summary>
        /// Gets or sets the passport number of the guest.
        /// </summary>
        public string PassportNumber
        {
            get { return passportNumber; }
            set 
            {
                if(value.Length <= 10)
                {
                    passportNumber = value;
                }
                else
                {
                    throw new Exception("Passport number can be maximum 10 digits long");
                }
            }
        }

        private int age;
        /// <summary>
        /// Gets or sets the age of the guest.
        /// </summary>
        public int Age
        {
            get { return age; }
            set 
            {
                if(value <= 101 && value > 0)
                {
                    age = value;
                }
                else
                {
                    throw new Exception("Age cannot be more than 101 or a negative number!");
                }
            }
        }
        #endregion

        public Guest(string name, string passportNum, int age) : base(name)
        {
            this.PassportNumber = passportNum;
            this.Age = age;
        }
    }
}
