﻿#region Summary
/*
 * Author: Asparuh Kamenov 02/12/2016
 * 
 * Part of the Prototype design pattern.
 * Description: The main window of the application. From here, bookings and customers can be added and ammended.
 * It has a datagrid which is connected to the "Bookings" collection in the "Storage" class, which contains
 * the recorded bookings. From there the user can pick a booking and choose to remove, ammend or print the receipt for.
 * It also contains the template for adding and ammending customers. When clicking the "Save" button if "currentCustomer" 
 * is null, a new Customer object is created with the values from the GUI. 
 * Else it retrieves a prototype object from the "Storage" class.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Customer currentCustomer;

        public MainWindow()
        {
            InitializeComponent();
        }

        #region Events
        //When the "Save" button is clicked
        private void btnCustomer_Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //If creating a new customer
                if (currentCustomer == null)
                {
                    string fullName = txtbCustomer_FirstName.Text + " " + txtbCustomer_LastName.Text;
                    currentCustomer = new Customer(fullName, txtbCustomer_Address.Text);

                    Storage.Customers.Add(currentCustomer);

                    currentCustomer = null;

                    MessageBox.Show("The customer has been added!", "Notification", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }
                else
                {
                    //Else replace the object in the collection in the "Storage" class.
                    string fullName = txtbCustomer_FirstName.Text + " " + txtbCustomer_LastName.Text;

                    currentCustomer.Name = fullName;
                    currentCustomer.Address = txtbCustomer_Address.Text;

                    int index = Storage.GetIndexAtID(DatabaseOptions.Customer, currentCustomer.ReferenceNumber);

                    Storage.Customers[index] = currentCustomer;

                    currentCustomer = null;

                    //Hide the "Cancel" button
                    btnCustomer_Cancel.Visibility = Visibility.Hidden;

                    //Refresh the datagrid to show the changes
                    dgBookings.Items.Refresh();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }

            clearCustomerTemplate();
        }

        //When the "Amend existing" button is clicked
        private void btnCustomer_Amend_Click(object sender, RoutedEventArgs e)
        {
            //Show the window, containing the existing customers
            SearchDisplayWindow window = new SearchDisplayWindow();
            window.ShowDialog();

            if (window.SelectedIndex >= 0)
            {
                //If a customer is picked, use the selecting index to retrieve the corresponding object in the collection
                //located in the "Storage" class and fill the boxes
                int index = window.SelectedIndex;

                currentCustomer = (Customer)Storage.Customers[index].Clone();

                string names = currentCustomer.Name;
                string[] splitNames = names.Split(new char[] { ' ' });

                txtbCustomer_FirstName.Text = splitNames[0];
                txtbCustomer_LastName.Text = splitNames[1];
                txtbCustomer_Address.Text = currentCustomer.Address;

                //Shows the button used to cancel an amendment
                btnCustomer_Cancel.Visibility = Visibility.Visible;
            }
        }

        //When the "Cancel" button is clicked when amending a customer
        private void btnCustomer_Cancel_Click(object sender, RoutedEventArgs e)
        {
            currentCustomer = null;
            btnCustomer_Cancel.Visibility = Visibility.Hidden;
            clearCustomerTemplate();
        }

        //When the "Add Booking" button is clicked
        private void btnBooking_Add_Click(object sender, RoutedEventArgs e)
        {
            //Bring up the booking window
            BookingWindow window = new BookingWindow();
            window.ShowDialog();
        }

        //When the "Amend Booking" button is clicked
        private void btnBooking_Amend_Click(object sender, RoutedEventArgs e)
        {
            //If a booking has been selected from a datagrid pass its index as an argument
            if(dgBookings.SelectedIndex != -1)
            {
                BookingWindow window = new BookingWindow(dgBookings.SelectedIndex);
                window.ShowDialog();
            }
            else
            {
                MessageBox.Show("Select a booking to amend.", "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        private void btnBooking_Remove_Click(object sender, RoutedEventArgs e)
        {
            //If a booking has been selected from the datagrid
            if (dgBookings.SelectedIndex != -1)
            {
                //Prompt the user before deleting
                MessageBoxResult prompt = MessageBox.Show("Delete booking?", "Prompt",
                       MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (prompt == MessageBoxResult.Yes)
                {
                    //Delete from the collection in the "Storage" class the corresponding index
                    Storage.Bookings.RemoveAt(dgBookings.SelectedIndex);
                }
            }
            else
            {
                MessageBox.Show("Select a booking to remove.", "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        //When the "Print receipt" button is clicked
        private void btnBooking_PrintReceipt_Click(object sender, RoutedEventArgs e)
        {
            //If a booking has been selected from the datagrid
            if (dgBookings.SelectedIndex != -1)
            {
                int index = dgBookings.SelectedIndex;

                //Pass the ID of the booking, the customer name associated with it, and the cost, using the GetCost() method
                ReceiptWindow window = new ReceiptWindow(Storage.Bookings[index].ReferenceNumber,
                    Storage.Bookings[index].CustomerName, Storage.Bookings[index].GetCost());
                window.ShowDialog();
            }
            else
            {
                MessageBox.Show("Select a booking to print the receipt for.", "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        //When exiting the application serialize the data to a file
        private void windowMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Storage.SaveCache();
        }

        //When opening the application load the data from the file
        private void windowMain_Loaded(object sender, RoutedEventArgs e)
        {
            Storage.LoadCache();
        }

        //Set up the datagrid on start up
        private void gridMain_Loaded(object sender, RoutedEventArgs e)
        {
            dgBookings.ItemsSource = Storage.Bookings;

            dgBookings.AutoGenerateColumns = false;

            DataGridTextColumn col1 = new DataGridTextColumn();
            Binding id = new Binding("ReferenceNumber");
            col1.Binding = id;
            col1.Header = "ID";
            dgBookings.Columns.Add(col1);

            DataGridTextColumn col2 = new DataGridTextColumn();
            Binding arrivalDate = new Binding("ArrivalDate");
            arrivalDate.StringFormat = "dd/MM/yyyy";
            col2.Binding = arrivalDate;
            col2.Header = "ArrivalDate";
            dgBookings.Columns.Add(col2);

            DataGridTextColumn col3 = new DataGridTextColumn();
            Binding departureDate = new Binding("DepartureDate");
            col3.Binding = departureDate;
            departureDate.StringFormat = "dd/MM/yyyy";
            col3.Header = "DepartureDate";
            dgBookings.Columns.Add(col3);

            DataGridTextColumn col4 = new DataGridTextColumn();
            Binding customerName = new Binding("CustomerName");
            col4.Binding = customerName;
            col4.Header = "CustomerName";
            dgBookings.Columns.Add(col4);

            //Set the last column of the datagrid to expand to the end of the datagrid
            dgBookings.Columns[dgBookings.Columns.Count - 1].Width =
                new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        //When the "Exit" button is clicked
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Methods
        //Helper function to clear the text boxes
        private void clearCustomerTemplate()
        {
            //Get the collection of text boxes in the grid
            IEnumerable<TextBox> textBoxes = gridMain.Children.OfType<TextBox>();
            //Clear each one of them
            foreach (TextBox element in textBoxes)
            {
                element.Clear();
            }
        }
        #endregion
    }
}