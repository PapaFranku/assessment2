﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: The base class from which objects which represent a person inherit. Has a property for the name.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    [Serializable]
    public abstract class Person
    {
        private string name;
        /// <summary>
        /// Gets or sets the person' name.
        /// </summary>
        public string Name
        {
            get { return name; }
            set 
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    name = value;
                }
                else
                {
                    throw new Exception("Name cannot be empty!");
                }
            }
        }

        public Person(string name)
        {
            this.Name = name;
        }
    }
}
