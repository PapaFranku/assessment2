﻿#region Summary
/*
 * Author: Asparuh Kamenov 25/11/2016
 * 
 * Description: A simple window representing the receipt of a given booking
 * It takes the booking id, the customer name associated with it and the total cost for that booking.
 * It forms a string with these values and writes it out to the text box.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for ReceiptWindow.xaml
    /// </summary>
    public partial class ReceiptWindow : Window
    {
        public ReceiptWindow(int id ,string name ,decimal cost)
        {
            InitializeComponent();

            string print = string.Format("The price for booking {0} with customer {1}, is: {2}£",
                id, name, cost);

            txtbReceipt.Text = print;
        }

        #region Events
        //When the "OK" button is clicked
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
