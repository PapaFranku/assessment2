﻿#region Summary
/*
 * Author: Asparuh Kamenov 02/12/2016
 * 
 * Part of the Prototype design pattern.
 * Description: A static class which serves as the "database" for the application. It represents the "registry" 
 * of the prototypical objects - part of the design pattern.
 * It has two collections: One for the customers and one for the bookings. The former is connected 
 * to the datagrid in the "MainWindow". The latter is connected to a datagrid in the "CustomerDisplayWindow". 
 * When the collections change, the changes are reflected in the datagrids as well.
 * GetIndexAt() retrieves the index of the object in one of the collections, 
 * provided an ID, since it is a unique identifier. GetID() is used by objects with an ID
 * to assign them one. LoadCache() and SaveCache() are used for serialization of the two lists to store the data
 * in them when exiting and loading the program.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace assessment2
{
    public static class Storage
    {    
        public static ObservableCollection<Booking> Bookings { get; set; }
        public static ObservableCollection<Customer> Customers { get; set; }

        static Storage()
        {
            Bookings = new ObservableCollection<Booking>();
            Customers = new ObservableCollection<Customer>();
        }

        #region Methods
        /// <summary>
        /// Gets the index of an object in database which corresponds to the ID provided.
        /// </summary>
        /// <param name="database"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int GetIndexAtID(DatabaseOptions database, int id)
        {
            //If searching in the "Customers" collection
            if(database == DatabaseOptions.Customer)
            {
                for(int i = 0; i < Customers.Count; i++)
                {
                    if(id == Customers[i].ReferenceNumber)
                    {
                        return i;
                    }
                }
            }
            else
            {
                //If searching in the "Bookings" collection
                for(int i = 0; i < Bookings.Count; i++)
                {
                    if(id == Bookings[i].ReferenceNumber)
                    {
                        return i;
                    }
                }
            }

            return -1;
        }

        /// <summary>
        /// Finds the object with the highest ID number and returns that number incremented by 1.
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        public static int GetID(DatabaseOptions database)
        {
            //If searching in the "Customers" collection
            if (database == DatabaseOptions.Customer)
            {
                int highestID = 0;

                foreach(Customer c in Customers)
                {
                    if(c.ReferenceNumber > highestID)
                    {
                        highestID = c.ReferenceNumber;
                    }
                }

                return highestID + 1;
            }
            else
            {
                //If searching in the "Bookings" collection
                int highestID = 0;

                foreach (Booking b in Bookings)
                {
                    if (b.ReferenceNumber > highestID)
                    {
                        highestID = b.ReferenceNumber;
                    }
                }

                return highestID + 1;
            }
        }

        /// <summary>
        /// Loads the data from the save file.
        /// </summary>
        public static void LoadCache()
        {
            if (File.Exists("data.bin"))
            {
                try
                {
                    using (Stream stream = File.Open("data.bin", FileMode.Open))
                    {
                        BinaryFormatter bin = new BinaryFormatter();
                        Storage.Bookings = (ObservableCollection<Booking>)bin.Deserialize(stream);
                        Storage.Customers = (ObservableCollection<Customer>)bin.Deserialize(stream);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                File.Create("data.bin");
            }
        }

        /// <summary>
        /// Saves the data from the two collections to a file.
        /// </summary>
        public static void SaveCache()
        {
            try
            {
                using (Stream stream = File.Open("data.bin", FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, Storage.Bookings);
                    bin.Serialize(stream, Storage.Customers);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
